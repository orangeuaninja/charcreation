from os import listdir
from os.path import isfile, join
import os
import shutil
import time
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

#set of file types
def getTypes(files):
    types = [i.split(".")[-1] for i in files]
    return set(types)

#create folders
def initFolers(fileTypes, file_path):
    for i in fileTypes:
        if os.path.isdir(join(file_path, i)) == False:
            os.mkdir(join(file_path, i))

#move file
def moveFiles(file_path, files):
    for i in files:
        #current directory and file for sorting
        if i.split(".")[-1] == "md" or i == "organize.py":
            continue

        dir = join(file_path, i.split(".")[-1])
        f = join(file_path, i)
        
        #check if directory doesn't exist and create it
        if os.path.isdir(dir) == False:
            os.mkdir(dir)

        #move file into directory
        shutil.move(f, dir)

class watch:
    DirWatch = '/home/orange/Desktop/CharCreation'

    def __init__(self):
        self.observer = Observer()
    
    def run(self):
        event_handler = handle()
        self.observer.schedule(event_handler, self.DirWatch, recursive=False)
        self.observer.start()
        try:
            while True:
                time.sleep(5)
        except:
            self.observer.stop()
            print("Error")
        self.observer.join()

class handle(FileSystemEventHandler):
    @staticmethod
    def on_any_event(event):
        if event.is_directory:
            return None

        elif event.event_type == 'created':
            # Take any action here when a file is first created.
            file_path = '/home/orange/Desktop/CharCreation'
            files = [f for f in listdir(file_path) if isfile(join(file_path, f))]
            moveFiles(file_path, files)

        elif event.event_type == 'modified':
           return None

if __name__ == '__main__':
    #w = watch()
    #w.run()

    file_path = '/home/orange/Desktop/CharCreation'
    files = [f for f in listdir(file_path) if isfile(join(file_path, f))]
    moveFiles(file_path, files)