#Testing info

#collecting spell page data starting with 1 spell
currSpell = (level0["Spell Name"][0]).replace(" ", "-").replace("/", "-")
if(currSpell.split('-')[-1].startswith("(")):
    currSpell = currSpell[:-5]


#grabs curr spell page

spellUrl = "http://dnd5e.wikidot.com/spell:"+currSpell
spellPage = requests.get(spellUrl)
spellSoup = BeautifulSoup(spellPage.text, 'html')

#print(spellUrl)
#print(spellSoup)
#print(currSpell)

content = spellSoup.find("div", id="page-content").findAll("p")
#print(len(content))

source = content[0]
spellList, hlevel = [],[]
s = len(content)
for i in range(len(content)):
    if  content[-i].text.find("Spell Lists") != -1:
        s = s-i
        spellList = content[-i].text.strip().replace(",", "").split(" ")[2:]
    if content[-i].text.find("At Higher Levels.") != -1:
        s = s-i
        hlevel = content[-i].text.strip()
        break
if(s != 3):
    effect = " ".join(i.text.strip()+'\n' for i in content[3:s])   
else:
     effect = content[3].text.strip()   

#print(source)
#print(effect)
#print(hlevel)
#print(spellList)